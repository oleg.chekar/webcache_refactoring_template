import json
from webcacheclient import WebCacheClient


def play_get_request():
    client = WebCacheClient()

    url_list = [f"https://nominatim.openstreetmap.org/search/Bank,{zip_code},Switzerland?osm_type=N&format=json"
                for zip_code in range(2095, 2199)]
    ret = client.fetch_urls(url_list, category="OSM-geocoding", output="json")
    print(ret)


def play_get_request_without_proxies():
    client = WebCacheClient()

    url_list = ["http://localhost:7070/search/Switzerland?osm_type=N&format=json",
                "http://localhost:7070/search/Germany?osm_type=N&format=json"]
    ret = client.fetch_urls(url_list, category="OSM-geocoding", output="json")
    print(ret)


def play_post_request():
    client = WebCacheClient()

    url_list = [f"https://search.wdoms.org?sSchoolName={med_school}&iPageNumber=1" for med_school in
                ['Basel', 'Zürich', 'London']]
    ret = client.fetch_urls(url_list, category="med_school", output="xml", method="POST")
    print(ret)


def play_post_data_request():
    client = WebCacheClient()

    url_list = [("https://search.wdoms.org", json.dumps({'sSchoolName': med_school, 'iPageNumber': 1}))
                for med_school in ['Basel', 'Zürich', 'London']]
    ret = client.fetch_urls(url_list, category="med_school", output="xml", method="POST")
    print(ret)


def get_proxy():
    client = WebCacheClient()
    print(client.get_proxy_list(10000))


if __name__ == '__main__':
    # play_post_request()
    # play_post_data_request()
    play_get_request()
    #play_get_request_without_proxies()
    # getProxy()
